import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pokemon_TCG/model/pokemon_model.dart';

const String url = "https://api.pokemontcg.io/v1/cards";

class API {
  static Future<List<PokemonModel>> getCards() async {
    var response = await http.get(url);
    var responseJson = jsonDecode(response.body)['cards'] as List;
    List<PokemonModel> pokelist = responseJson.map((e) => PokemonModel.fromJSON(e)).toList();
    return pokelist;
  }
}
