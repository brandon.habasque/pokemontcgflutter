import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pokemon_TCG/model/pokemon_model.dart';

class DetailsPage extends StatelessWidget {
  final PokemonModel pokemon;

  DetailsPage({Key key, @required this.pokemon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.pop(context),
          ),
          title: Text(pokemon.name)),
      body: Center(
        child: Column(
          children: [
            CachedNetworkImage(
              imageUrl: pokemon.imageUrl,
            )
          ],
        ),
      ),
    );
  }
}
