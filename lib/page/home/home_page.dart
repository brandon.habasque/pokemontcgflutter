import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pokemon_TCG/api/pokemon_api.dart';
import 'package:pokemon_TCG/model/pokemon_model.dart';
import 'package:pokemon_TCG/page/details/details_page.dart';

class HomePage extends StatefulWidget {
  @override
  createState() => _PokelistState();
}

class _PokelistState extends State {
  List<PokemonModel> pokelist = [];

  _getCards() {
    API.getCards().then((value) {
      setState(() {
        pokelist = value;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _getCards();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Pokémon TCG - Brandon"),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(height: 16.0),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: TextFormField(
                  decoration: const InputDecoration(
                hintText: 'Rechercher un pokémon',
              )),
            ),
            Container(height: 16.0),
            Expanded(
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: pokelist.length,
                itemBuilder: (context, index) {
                  return Container(
                    child: TextButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) {
                            return DetailsPage(
                              pokemon: pokelist[index],
                            );
                          }),
                        );
                      },
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Column(
                            children: [
                              Text(pokelist[index].name),
                              Container(height: 8.0),
                              CachedNetworkImage(
                                imageUrl: pokelist[index].imageUrl,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ));
  }
}
