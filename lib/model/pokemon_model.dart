class PokemonModel {
  String name;
  String imageUrl;
  // String text;
  PokemonModel(this.name, this.imageUrl);

  factory PokemonModel.fromJSON(dynamic json) {
    return PokemonModel(json['name'] as String, json['imageUrl'] as String);
  }
}
